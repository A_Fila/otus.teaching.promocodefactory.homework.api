import { Route, Routes } from "react-router-dom"
import { Home } from "./pages/Home"
import { Page404 } from "./pages/Page404"
import { Container } from "react-bootstrap"
import { Navbar } from "./components/Navbar"
import SignalR from "./pages/SignalR"
import GRPC from "./pages/GRPC"

import 'bootstrap/dist/css/bootstrap.min.css';
function App() { 

    return (
        <>
        <Navbar/>
            <Container className="mb-4">
            
          <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/signalr" element={<SignalR />} />
              <Route path="/grpc" element={<GRPC />} />

              <Route path="*" element={<Page404 />} />

          </Routes>
            </Container>
        </>
  )
}

export default App
