export function Home() {
    return(
    <>
            <h2 className="learning-near__header">Домашнее задание №12</h2>

                <div className="text text_p-small text_default learning-markdown js-learning-markdown"><p>Добавить один из инструментов к своему проекту</p> </div>
            <div className="text text_p-small text_default text_bold fw-bold">Цель:</div>
                <div className="text text_p-small text_default learning-markdown js-learning-markdown"><p>Данное задание позволит попробовать технологии альтернативные REST-like подходу для формирования API сервисов.</p> </div>

            <br />
            <div className="text text_p-small text_default text_bold fw-bold">
                Описание/Пошаговая инструкция выполнения домашнего задания:
            </div>
                <div className="text text_p-small text_default learning-markdown js-learning-markdown">
                    <p>Сделать форк репозитория Homework 5
                        (<a target="_blank" href="https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Homework-5" title="https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Homework-5">
                                https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Homework-5
                        </a>) домашнего задания и реализовать пункты в нем;

                        <br />

                    Реализуем API аналогичный Customers с помощью 2-х разных подходов из технологий: <a href="/grpc">gRPC</a>, GraphQL или <a href="/signalr">SignalR</a>; Например, GRPC и GraphQL.
                    </p>

                    <br />
                <div className="text text_p-small text_default text_bold fw-bold">Критерии оценки:</div>
                    <div className="text text_p-small text_default learning-markdown js-learning-markdown"><p>Api реализован на 1-й технологии - 6 баллов;
                        <br />

                        Api реализован на 2-х технологиях - 10 баллов;

                        <br />

                        Минимальный проходной порог: 6 баллов.</p>
                    </div>

                    <br />

                    <div className="text text_p-small text_default">Рекомендуем сдать до: </div>
                    <div className="text text_p-small text_default">Статус: не сдано  </div>

                </div>                                 
    </>
    )
}