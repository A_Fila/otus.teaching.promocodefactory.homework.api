export function Page404() {
    const style = { color: 'red' };
    return <h1 style={ style } >Page not found</h1>
}