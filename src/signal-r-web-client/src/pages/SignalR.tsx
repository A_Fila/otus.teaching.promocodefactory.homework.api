import { HttpTransportType, HubConnection, HubConnectionBuilder, LogLevel } from "@microsoft/signalr";
import { useCallback, useEffect, useMemo, useRef, useState } from "react"

import { AgGridReact } from 'ag-grid-react'; // React Data Grid Component
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the grid
import "ag-grid-community/styles/ag-theme-quartz.css"; // Optional Theme applied to the grid
import { RowSelectedEvent, CellValueChangedEvent } from "@ag-grid-community/core";

import { Button } from "react-bootstrap";
import ListPreferences from "../components/ListPreferences";
import CustomerForm from "../components/CustomerForm";

//type Customer = {
//    firstName?: string;
//    lastName?: string;
//    fullName?: string;
//    email?: string;
//    preferences: CustomerPreference[]
//}

//type CustomerPreference = {
//    customerId: string;
//    preferenceId: string;
//    preference: PreferenceResponse;

//}

type CustomerShortResponse = {
    id: string;
    firstName?: string;
    lastName?: string;
    email?: string;
}

export type CreateOrEditCustomerRequest ={
    firstName?: string;
    lastName?: string;
    email?: string;
    preferenceIds: string[];
}

export type CustomerResponse = {
    id: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    preferences: PreferenceResponse[];
    promoCodes: PromoCodeShortResponse[];
}

export type PreferenceResponse = {
    id: string;
    name: string;
}

export type PromoCodeShortResponse = {
    id: string;
    code: string;
    serviceInfo: string;
    beginDate: string;
    endDate: string;
    partnerName: string;
}


const CUSTOMERS_API = 'http://localhost:5000/hubs/customers';// http://localhost:5000/hubs/customers'; //'https://localhost:5001/api/v1/customers'

const SignalR = () => {
    const [conn, setConn] = useState<HubConnection | null>(null);
    //const [isConnected, setIsConnected] = useState(false);
    const [modalShow, setModalShow] = useState<boolean>(false);
    //const [refreshListPrefs, setRefreshListPrefs] = useState(null);

    //const [curCustomerId, setCurCustomerId] = useState<string | null>(null);
    const [customerList, setCustomerList] = useState<CustomerShortResponse[] | null>(null);
    const [curCustomer, setCurCustomer] = useState<CustomerResponse | null>(null);
    const [prefs, setPrefs] = useState<PreferenceResponse[]>([]);

    const gridRef = useRef<AgGridReact>(null);
    
    //<ColDef<CustomerShortResponse[]>>
    const colDefs = useMemo( () => 
        [
            { field: "id", headerName: "id", flex: 3, cellRenderer: 'agGroupCellRenderer' },
            { field: "firstName", headerName: "First name", flex: 1, editable: true},
            { field: "lastName", headerName: "Last name", flex: 1 },
            { field: "email", headerName: "E-mail", flex: 2 },

            {
                field: "preferences", headerName: "preferences", cellRenderer: ListPreferences,
                cellRendererParams: {
                    connection: conn,
                    //prefList : 'Ы'
                    //setPrefs: (listOfPrefs: string) => {return  listOfPrefs}
                },
                flex: 1
            },

        ], [conn]);

    useEffect(() => {
        const signalrConn = new HubConnectionBuilder()
            .withUrl(CUSTOMERS_API, { transport: HttpTransportType.WebSockets | HttpTransportType.LongPolling })
            .configureLogging(LogLevel.Information)
            .withAutomaticReconnect()//!!!!!!!!!!!!!!!!
            .build();

        setConn(signalrConn);
        
        //console.log('signalrConn.serverTimeoutInMilliseconds=', signalrConn.serverTimeoutInMilliseconds);
        signalrConn.serverTimeoutInMilliseconds = 2 * 60 * 1000; //30 sec by default    

        if (signalrConn) {
            signalrConn.start()
                .then(() => {
                    console.log(`${new Date().toISOString()} connected with SignalR, state=${signalrConn.state}`);
                    //signalrConn.on('ReceiveAllOnClient', (customers: CustomerShortResponse[]) => {
                    //    console.log(`${new Date().toISOString()} customers=`, customers);
                    //    setCustomerList(customers);
                    // });

                    signalrConn.invoke('GetAllPreferences')
                        .then((res: PreferenceResponse[]) => {
                            console.log('preferences=', res);
                            setPrefs(res);
                        })
                        .catch(err => {
                            console.error('GetAllPreferences:', err);
                        })

                    signalrConn.onclose((err) => {
                        //setIsConnected(false);
                        console.log('onClose():', err)
                    });

                    signalrConn.onreconnected(() => console.log('*********RECONNECTED***********'));

                    signalrConn.on('DeleteOnClient', (id: string) => {
                        console.log(`${new Date().toISOString()} DeleteOnClient(): id=`, id);
                        const node = gridRef.current!.api.getRowNode(id);
                        //console.log(`${new Date().toISOString()} DeleteOnClient()`, node);
                        if (node)
                            gridRef.current!.api.applyTransaction({ remove: [node] })
                    });

                    //add to AG Grid new Customer
                    signalrConn.on('CreateOnClient', (customer: CustomerResponse) => {
                        console.log(`${new Date().toISOString()} CreateOnClient(): customer=`, customer);
                        const node = gridRef.current!.api.getRowNode(customer.id);

                        if (!node) {

                            const gridCust = {
                                ...customer,
                                preferences: customer.preferences
                                    .map(p => p.name)
                                    .join(', ')
                            }

                            //console.log('gridCustomer', { ...gridCust });

                            //map customer
                            const res = gridRef.current!.api.applyTransaction({
                                add: [{ ...gridCust}]
                            });
                            console.log(`Customer '${customer.firstName}'(id=${customer.id}) added to grid!`)
                        }
                        else {
                            console.log(`Customer with id=${customer.id} already added to grid!`)
                        }
                    });

                    //update AG Grid to view customer changes
                    //signalrConn.on('EditOnClient', (id: string, customer: CreateOrEditCustomerRequest) => {
                    signalrConn.on('EditOnClient', (customer: CustomerResponse) => {
                        console.log(`${new Date().toISOString()} EditOnClient(): customerId='${customer.id}'`, customer);
                        const node = gridRef?.current?.api.getRowNode(customer.id);//'a6c8c6b1-4349-45b0-ab31-244740aaf0f0'

                        if (!node) {
                            console.log(`EditOnClient(): customerId='${customer.id}'' not found in grid`)
                        }
                        else {                            
                            node.updateData({ ...customer });//id,
                            //console.log('prefs=', prefs);
                            const prefList = customer.preferences
                                .map(p => p.name)
                                .join(', ');//prefs.find(p => p.id === id)?.name

                            console.log('prefs=', prefList);
                            node.setDataValue("preferences", prefList)
                            //gridRef?.current?.api.redrawRows();                           

                            console.log(`EditOnClient(): customerId='${customer.id}' UPDATED!!!`)
                        }
                    });

                    //get All customers from server
                    signalrConn.invoke('GetAll')
                        .then((res: CustomerShortResponse[]) => {
                            console.log("GetAll", res)
                            setCustomerList(res);
                            //setIsConnected(true);
                        })
                        .catch(err => {
                            console.error('GetAll:', err);
                        });
                })
                .catch(err => {
                    console.error('connectionError:', err);
                });
        }
        else {
            console.error(`${new Date().toISOString()} useEffect(): variable 'signalrConn' not defined`);    
        }

        return () => {
            console.log(`${new Date().toISOString()} useEffect() return`);
            if (signalrConn) {
                //console.log(`${new Date().toISOString()} disconnected`);
                //signalrConn.stop(); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            }
        }
        
    }, []);

    const onRowSelected = async (event: RowSelectedEvent) => {
        //console.log('onRowSelected():', `rowId=${event.node.data.id} selected = ${event.node.isSelected()}`);  //window.alert(msg);
        if (event.node.isSelected()) {
            console.log('customerId=', event.node.data.id);
            //setCurCustomerId(event.node.data.id);
        }
    }

    const onCellChanged = async (event: CellValueChangedEvent) => {
        console.log(event)
        console.log(`New Cell Value: ${event.value}`)
    }
    const onRemoveSelected = useCallback(() => {
        const selectedData = gridRef.current!.api.getSelectedRows();
        if (selectedData.length === 0) return;
        
        //delete customer on server
        conn!.invoke('Delete', selectedData[0].id)
            .catch(err => {
                console.error('invoke Delete():', err);
            });

        //delete row on client
        const res = gridRef.current!.api.applyTransaction({ remove: selectedData, });
        console.log(res);

    }, [conn]);

    const onAdd = useCallback(() => {
        setCurCustomer(null)
        //open modal form
        setModalShow(true);
        ////add new customer on server
        //conn!.invoke('Add', selectedData[0].id)
        //    .catch(err => {
        //        console.error('invoke Delete():', err);
        //    });

        ////add row on client
        //const res = gridRef.current!.api.applyTransaction({ remove: selectedData, });
        //console.log(res);

    }, [conn]);

    const onEdit = () => {
        
        const selectedData = gridRef.current!.api.getSelectedRows();
        if (selectedData.length === 0) return;

        //const preferences = gridRef?.current?.api.getRowNode('a6c8c6b1-4349-45b0-ab31-244740aaf0f0');//.getRenderedNodes()
        //console.log('preferences',preferences)
        conn!.invoke('GetById', selectedData[0].id)
        .then(res => {
            console.log(res);
            setCurCustomer(res);
        })
        .catch(err => {
            setCurCustomer(null);
            console.error('GetById:', err);
        });
        
        //console.log();
        //open modal form
        setModalShow(true);
        ////add new customer on server
        //conn!.invoke('Add', selectedData[0].id)
        //    .catch(err => {
        //        console.error('invoke Delete():', err);
        //    });

        ////add row on client
        //const res = gridRef.current!.api.applyTransaction({ remove: selectedData, });
        //console.log(res);

    }

    const AddCustomer = (customer: CreateOrEditCustomerRequest) =>
    {
        console.log('customer=', customer);
        //create new customer on server
        conn?.invoke('CreateCustomer', customer)
            .then((res: CustomerShortResponse) => {//CustomerShortResponse
                //console.log(res);
                //...add to grid
            })
            .catch(err => {
                console.error('CreateCustomer:', err);
            });
        
    }

    const EditCustomer = (id: string, customer: CreateOrEditCustomerRequest) => {
        console.log('customer=', customer);
        //edit customer on server
        conn?.invoke('EditCustomer', id, customer)
            .then((res: CustomerResponse) => {//
                console.log(`${new Date().toISOString()} EditCustomer result:`, res);
            })
            .catch(err => {
                console.error('EditCustomer:', err);
            });
    }

    // provide Detail Cell Renderer Params
    //const detailCellRendererParams = {
    //    detailGridOptions: {
    //        columnDefs: [
    //            { field: 'id' },
    //            { field: 'name' },                
    //        ]
    //    },
    //    // get the rows for each Detail Grid
    //    getDetailRowData: params => {
    //        console.log('getDetailRowData', params)
    //        params.successCallback(params.data.preferences);
    //    }
    //};

    //const getRowId = useCallback(
    //    (params: GetRowIdParams<CustomerShortResponse>) => params.data.id,
    //    [],
    //);

    return <>
        <h1>SignalR</h1>
        <Button className="my-4 me-4" variant="success" onClick={onAdd}>Add new</Button>
        <Button className="my-4 me-4" variant="warning" onClick={onEdit}>Edit</Button>
        <Button className="my-4" variant="danger" onClick={onRemoveSelected}>Delete selected</Button>
        <div
            className="ag-theme-quartz" // applying the grid theme
            style={{ height: '80vh', width:'100%' }} // the grid will fill the size of the parent container
        >
            <AgGridReact
                ref={gridRef}
                rowData={customerList}
                columnDefs={colDefs}

                rowSelection="single"
                onRowSelected={onRowSelected}
                onCellValueChanged={onCellChanged}
                getRowId={params => params.data.id}
                //masterDetail={true} /*enterprise*/
                //detailCellRendererParams={detailCellRendererParams}
                
            />            
        </div>
        <CustomerForm
            show={modalShow}
            onHide={() => setModalShow(false)}
            onAddCustomer={AddCustomer}
            onEditCustomer={EditCustomer }
            //conn={conn}
            //isConnected={isConnected}
            prefs={prefs}
            customer={curCustomer}
        />
    </>
}
export default SignalR;
