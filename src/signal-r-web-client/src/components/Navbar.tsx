import { Container, Nav, Navbar as NavbarBs } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
//import { }
export function Navbar() {
    return (
        <NavbarBs className="bg-white shadow-sm mb-3">
            <Container >
            <Nav>
                <Nav.Link to="/" as={NavLink}>Home</Nav.Link>
                <Nav.Link to="/signalr" as={NavLink}>SignalR</Nav.Link>
                    <Nav.Link to="/grpc" as={NavLink}>gRPC</Nav.Link>
                </Nav>
            </Container>
        </NavbarBs>
    )
}