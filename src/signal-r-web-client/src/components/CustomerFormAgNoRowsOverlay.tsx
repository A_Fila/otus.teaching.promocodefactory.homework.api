import { CustomNoRowsOverlayProps } from "ag-grid-react";

export default ( props: CustomNoRowsOverlayProps & { noRowsMessageFunc: () => string },) => {
    return (
        <div            
            className="ag-overlay-loading-center"
            style={{ backgroundColor: "orange" }}
        >
            <i>
                {props.noRowsMessageFunc()}
            </i>
        </div>
    );
};