import { AgGridReact } from 'ag-grid-react'; // React Data Grid Component
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the grid
import "ag-grid-community/styles/ag-theme-balham.css"; // Optional Theme applied to the grid

import { ChangeEvent, useCallback, useEffect, useMemo, useRef, useState } from "react";
import { Button, Col, Container, Form, Modal, Row } from "react-bootstrap";
import CustomerFormAgNoRowsOverlay from './CustomerFormAgNoRowsOverlay';
import { CreateOrEditCustomerRequest, CustomerResponse, PreferenceResponse } from '../pages/SignalR';
import DeletePreference from './DeletePreference';


//type Preferenceesponse = {
//    id: string;
//    name: string;
//}
type CustomerFormProps = {
    show: boolean;
    onHide: () => void;
    prefs: PreferenceResponse[];
    onAddCustomer: (customer: CreateOrEditCustomerRequest) => void
    customer: CustomerResponse | null
    onEditCustomer: (id: string, customer: CreateOrEditCustomerRequest) => void
}
export default function CustomerForm(props: CustomerFormProps) {
    //const [prefs, setPrefs] = useState<PreferenceResponse[]>([]);
    const gridRef = useRef<AgGridReact>(null);
    const refPref = useRef<HTMLSelectElement>(null);
    const refFirstName = useRef<HTMLInputElement>(null);
    const refLastName = useRef<HTMLInputElement>(null);
    const refEmail= useRef<HTMLInputElement>(null);
    //const [customer, setCustomer] = useState<CustomerResponse| null>(null);
    //console.log('CUSTOMER_FORM,', props.prefs);
    //console.log('props=', props)

    //useEffect(() => {
    //    setCustomer(props.customer)
    //}, [props.customer]);

    useEffect(() => {
        //console.log(`STATE=${props.conn?.state} ${new Date().toISOString()}`);
        //console.log(props.conn);
        //console.log(`isConnected=${props.isConnected}`);
        //console.log("------------------------------");
        //get list of preferences from server
        //if (props.isConnected === true)//
        //    props.conn.invoke('GetAllPreferences')
        //            .then((res: PreferenceResponse[]) => {
        //                console.log('preferences=', res);
        //                setPrefs(res);
        //            })
        //            .catch(err => {
        //                console.error('GetAllPreferences:', err);
        //            });
    }, [props.isConnected]);
    const [IsBtnPlusDisabled, setIsBtnPlusDisabled] = useState(true);
    //const handleOnPrefDeleteClick = () => { //e: React.MouseEvent<HTMLButtonElement>
    //    console.log('click')
    //   // console.log(id)
    //}

    //const [formData, setFormData] = useState({
    //    FirstName: '',
    //    LastName: '',
    //    email: '',
    //    prefs: []
    //})

    const handleSaveClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        console.log(event);
        //get preferences from AG Grid
        const preferences = gridRef?.current?.api.getRenderedNodes()
        console.log('preferences=', preferences);

        const customer: CreateOrEditCustomerRequest = {
            firstName: refFirstName.current?.value,
            lastName: refLastName.current?.value,
            email: refEmail.current?.value,
            preferenceIds: (preferences?.map((p) => p.id) as string[]) 
        };

        if (props.customer?.id)
            props.onEditCustomer(props.customer.id, customer);
        else
            props.onAddCustomer(customer);

        props.onHide();        
    }

    //const handleOnFormControlChanged = (e) => {handleOnFormControlChanged
    //    //const { ctrlName, ctrlValue } = currentTarget;
    //    console.log('handleOnFormControlChanged', e)
    //    setFormData({
    //        ...formData,
    //        [e.target.name]: e.nativeEvent.data
    //    })
    //}

    const colDefs = useMemo(() =>
        [
            {
                headerName: "action",
                flex: 1,
                cellRenderer: DeletePreference,
                cellRendererParams: {
                    onClick:  (id) => {console.log('CLICK!' + id) }
                    //handleOnPrefDeleteClick
                }                
            },

            { field: "id", headerName: "id", flex: 4},
            { field: "name", headerName: "Name", flex: 1 },
            
        ], []);

    //const [preferences, setPreferences] = useState<PreferenceResponse[]>([])
    //const onAddNew = useCallback(() => {
    //     //delete customer on server
    //    props.conn.invoke('CreateCustomer', {
    //        FirstName: 'AAA',
    //        LastName: 'BBB',
    //        Email: 'a@b.cc',
    //        PreferenceIds: ["76324c47-68d2-472d-abb8-33cfa8cc0c84", "c4bda62e-fc74-4256-a956-4760b3858cbd" ]
    //    })
    //        .catch(err => {
    //            console.error('invoke Delete():', err);
    //        });

    ////    //delete row on client
    ////    const res = gridRef.current!.api.applyTransaction({ remove: selectedData, });
    //    console.log('saving customer to DB...');

    //}, []);

    const handlePrefOnChange = (e: ChangeEvent<HTMLSelectElement>) => {
        //console.log('inx=', e.target.selectedIndex);
        setIsBtnPlusDisabled(e.target.selectedIndex === 0)
    }

    const handlePrefOnAdd = (e: React.MouseEvent<HTMLButtonElement>) => {
        const res = gridRef.current!.api.applyTransaction({
            add: [{
                    id: refPref.current?.value,
                    name: refPref.current?.selectedOptions[0].text
            }]
        });
        //console.log(res);

        //console.log('refSel=', refSel.current?.selectedOptions);
    //    setPreferences([
    //        ...preferences, {  id: refSel.current?.value,
    //                           name: refSel.current?.selectedOptions[0].text
    //                        }
    //    ])
    }        

    //const noRowsOverlayComponent = useMemo(() => {
    //    return CustomerFormAgNoRowsOverlay;
    //}, []);

    //const noRowsOverlayComponentParams = useMemo(() => {
    //    return {
    //        noRowsMessageFunc: () => "No preferences has been added"
    //    };
    //}, []);

    return (
        <Modal show={props.show}    onHide={props.onHide}       //{...props}
            aria-labelledby="contained-modal-title-vcenter">
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    New customer
                </Modal.Title>
            </Modal.Header>
            <Modal.Body >
                <Form>
                    <Form.Group as={Row} className="mb-3" controlId="customerForm.FirstName">
                        <Form.Label column="sm" sm={3} >First name</Form.Label>
                        <Col sm={9} >
                            {// Re-render DefaultValue
                             // React will never update the input tag if the value passed to defaultValue changes.
                             // It was meant to be a default/starting value only 
                            }
                            <div key={props.customer?.firstName ?? ''}>
                                <Form.Control
                                    ref={refFirstName}
                                    name="FirstName"
                               
                                    //value={customer?.firstName ?? ''}
                                    //onChange={handleOnFormControlChanged }
                                    size="sm"
                                    type="text"
                                    placeholder="First name"
                                    required
                                    autoFocus
                                    defaultValue={ props.customer?.firstName ?? ''}
                                />
                            </div>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} className="mb-3" controlId="customerForm.LastName">
                        <Form.Label column="sm" sm={3}>Last name</Form.Label>
                        <Col sm={9} >
                            <div key={props.customer?.firstName ?? ''}>
                                <Form.Control
                                    //value={formData.LastName}
                                    //onChange={handleOnFormControlChanged}
                                    ref={refLastName}
                                    size="sm"
                                    type="text"
                                    placeholder="Last name"
                                    defaultValue={props.customer?.lastName ?? ''}
                                />
                            </div>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} className="mb-3" controlId="customerForm.Email">
                        <Form.Label column="sm" sm={3}>Email address</Form.Label>
                        <Col sm={9} >
                            <div key={props.customer?.firstName ?? ''}>
                                <Form.Control
                                    //value={formData.email}
                                    //onChange={handleOnFormControlChanged}
                                    ref={refEmail}
                                    size="sm"
                                    type="email"
                                    placeholder="name@example.com"
                                    defaultValue={props.customer?.email ?? ''}
                                />
                             </div>
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} className="mb-3" controlId="customerForm.PrefGrid">
                        <Form.Label column="sm" sm={2} >Preferences</Form.Label>
                        <Col sm={10} >
                            <div className="ag-theme-balham"  id="gridContainer"// applying the grid theme
                                style={{ height: '11vh',  width: '100%' }} // the grid will fill the size of the parent container
                            >
                                <AgGridReact
                                    ref={gridRef}
                                    columnDefs={colDefs}
                                    rowData={props.customer?.preferences ?? []}
                                    overlayNoRowsTemplate={'No preferences has been added'}
                                    getRowId={params => params.data.id}
                                //noRowsOverlayComponent={noRowsOverlayComponent}
                                //noRowsOverlayComponentParams={noRowsOverlayComponentParams}
                                />
                            </div>
                        </Col>
                    </Form.Group>


                    <Form.Group as={Row} className="mb-3" controlId="customerForm.Prefs">
                        <Form.Label column="sm" sm={3}></Form.Label>
                        <Col sm={6} >
                            <Form.Select size="sm" aria-label="Default select example"
                                ref={refPref}
                                onChange={handlePrefOnChange}
                            >
                                <option>Select preference to add</option>
                                {props.prefs.map( (p: PreferenceResponse)  =>
                                                    <option value={p.id} key={p.id}>
                                                        {p.name}
                                                    </option>
                                                 )
                                }
                            {/*    <option value="1">One</option>*/}
                            {/*    <option value="2">Two</option>*/}
                            </Form.Select>

                        </Col>
                        <Col sm={2}>
                            <Button variant="success" size="sm" title="add preference"
                                disabled={IsBtnPlusDisabled}
                                onClick={handlePrefOnAdd}
                            >
                                &nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;&nbsp;&nbsp;
                            </Button>
                        </Col>

                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="danger" onClick={props.onHide}>Cancel</Button>
                <Button variant="success" onClick={handleSaveClick}>Save</Button>
            </Modal.Footer>
        </Modal>
    );
}