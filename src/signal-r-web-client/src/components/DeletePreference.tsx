/* eslint-disable react-refresh/only-export-components */
import { CustomCellRendererProps } from "ag-grid-react";
import { Button } from "react-bootstrap";

interface DeletePreferenceParams extends CustomCellRendererProps {
    onClick: (param1: string) => void //(id:string
}

export default (params: DeletePreferenceParams) => {
    //console.log(params.data.id)
    const handleOnClick = (e: React.MouseEvent<HTMLButtonElement>) => {
        console.log(params);

        //const selectedData = params.api.getSelectedRows();
        //if (selectedData.length === 0) return;

        //delete row on client
        const res = params.api.applyTransaction({ remove: [params.data] });
        console.log(res);

       // console.log(e)
    }
    //const f = params.onClick;
    return (
        <Button onClick={handleOnClick/*params.onClick*/}
            variant="danger"
            size="sm"
            className="fw-bold"
            style={{
                height: '2vh',
                //width: '100%'
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
            }}
        >
            &nbsp;-&nbsp;
        </Button>

    )
}