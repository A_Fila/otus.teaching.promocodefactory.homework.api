import { HubConnection } from "@microsoft/signalr";
import { CustomCellRendererProps } from "ag-grid-react";
import { useState } from "react";

interface PreferencesCellRendererProps extends CustomCellRendererProps {
    connection: HubConnection;
    //setPref?: (params: string) => string;
    //prefList: string
}

export default function ListPreference(params: PreferencesCellRendererProps )  {
    //console.log(`${params.data.firstName}(${params.data.id})`);
    const [prefs, setPrefs] = useState<string>();
    //console.log('params=', params.data.preferences);

    //console.log('prefList=', params.prefList);
    //console.log('connection=', params.connection);

    const onPrefsClick = (e: React.MouseEvent<HTMLAnchorElement>): void => {
        console.log('onClick', params.connection);
        e.preventDefault();

        //get customer by id from server
        DownloadPrefs();
    }
    function DownloadPrefs(){
        params.connection.invoke('GetById', params.data.id)
            .then(res => {
                console.log(res);

                const strPrefs = res.preferences
                    .map(p => p.name)
                    .join(', ');

                setPrefs(strPrefs);
            })
            .catch(err => {
                console.error('GetById:', err);
            });
    }
    //useEffect(() => {
    //    params.setRefreshFunc(() => DownloadPrefs);
    //    console.log('set')
    //}, []);

    return (
        params.data.preferences ??
        prefs ??        
        <a href="" onClick={onPrefsClick} title='show preferences'>{'\u2B73'}</a>        
    )
}