﻿using Castle.Core.Resource;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Hubs
{
    public interface ICustomersClient
    {
        //Task ReceiveAllOnClient(List<CustomerShortResponse> customers);
       // Task ReceiveOnClient(CustomerResponse customer);
        Task DeleteOnClient(string id);
        Task CreateOnClient(CustomerResponse customer);
        Task EditOnClient(CustomerResponse customer);// string id, CreateOrEditCustomerRequest customer);
    }

    public class CustomersHub: Hub<ICustomersClient>
    {
        //private readonly IRepository<Customer> _customerRepository;
        //private readonly IRepository<Preference> _preferenceRepository;

        private readonly CustomerService _customerSrv;

        public CustomersHub(//IRepository<Customer> customerRepository,
                            //IRepository<Preference> preferenceRepository,
                            CustomerService customerService)
        {
            //_customerRepository = customerRepository;
            //_preferenceRepository = preferenceRepository;
            _customerSrv = customerService;

        }
        public async Task<List<CustomerShortResponse>> GetAll() 
        {
            return await _customerSrv.GetCustomersAsync();
            //Clients.Caller.ReceiveAllOnClient(customers);
        }

        public async Task<CustomerResponse> GetById(string id)
        {
            var customer = await _customerSrv.GetCustomerAsync(Guid.Parse(id));
            if (customer == null) throw new Exception($"Customer with id={id} not found");

            return new CustomerResponse(customer);
        }
        public async Task<List<PreferenceResponse>> GetAllPreferences()
        {
            return await _customerSrv.GetPreferencesAsync();
            //Clients.Caller.ReceiveAllOnClient(customers);
        }
        public async Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request)
        {
            var customer =  new CustomerResponse(await _customerSrv.CreateCustomerAsync(request));

            //Clients.Others.CreateOnClient(customer);
            //Clients.Caller.CreateOnClient(customer);
            Clients.AllExcept(Context.ConnectionId).CreateOnClient(customer);

            return customer;
        }

        public async Task<CustomerResponse> EditCustomer(string id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerSrv.EditCustomersAsync(Guid.Parse(id), request);

            CustomerResponse response = null;
            if (customer != null)
            {
                response = new CustomerResponse(customer);
                Clients.Others.EditOnClient(response);// id, request, customer.Preferences);
            }

            return response;
        }

        public async Task Delete(string id)
        {           
            if (await _customerSrv.DeleteCustomerAsync(Guid.Parse(id)))
                Clients.Others.DeleteOnClient(id);
        }


        //public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        //{
        //    var customers = await _customerRepository.GetAllAsync();

        //    var response = customers.Select(x => new CustomerShortResponse()
        //    {
        //        Id = x.Id,
        //        Email = x.Email,
        //        FirstName = x.FirstName,
        //        LastName = x.LastName
        //    }).ToList();

        //    return Ok(response);
        //}
    }
}
