using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Gateways;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Integration;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using Otus.Teaching.PromoCodeFactory.WebHost.Hubs;
using Microsoft.AspNetCore.Http.Connections;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{    
    public class Startup
    {
        //const string CLIENTS_CORS_POLICY_NAME = "ClientCorsPolicy";

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x=> 
                x.SuppressAsyncSuffixInActionNames = false);
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<INotificationGateway, NotificationGateway>();
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddDbContext<DataContext>(x =>
            {
                x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
                //x.UseNpgsql(Configuration.GetConnectionString("PromoCodeFactoryDb"));
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
                x.LogTo(Console.WriteLine, Microsoft.Extensions.Logging.LogLevel.Information);
            });

            //SignalR 
            //services.AddSignalR(configure =>
            //{   //for all hubs
            //    configure.EnableDetailedErrors = true;
            //    configure.KeepAliveInterval = TimeSpan.FromSeconds(240);
            //});

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });

            //CORS default policy
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(policy =>
                {
                    policy.WithOrigins("http://localhost:5173") //doesn't work with UseHttpsRedirect()
                    .AllowAnyHeader()
                    .AllowCredentials()
                        .AllowAnyMethod();
                        //.SetIsOriginAllowed(origin => true); // = any origin
                });
            });
            //services.AddCors(options =>
            //{
            //    options.AddPolicy(name: CLIENTS_CORS_POLICY_NAME,
            //        corsBuilder =>
            //        {
            //            corsBuilder.WithOrigins(Configuration.GetSection("CORS:Origins").Get<string[]>())
            //                       .WithHeaders(Configuration.GetSection("CORS:Headers").Get<string[]>())
            //                       .WithMethods(Configuration.GetSection("CORS:Methods").Get<string[]>());

            //        });

            //});

            //SignalR 
            services.AddSignalR(configure =>
            {   //for all hubs
                configure.EnableDetailedErrors = true;
                configure.KeepAliveInterval = TimeSpan.FromSeconds(240);
            });

            services.AddScoped<CustomerService>();

            //add gRPC
            services.AddGrpc().AddJsonTranscoding();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }


            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            //when using Cors middleware, it comes before UseHttpsRedirection.
            //if not, Cors middleware doesn't work!!!!!!!!!!!!!!!!!!!!!!!!!!
            //app.UseHttpsRedirection();

            app.UseRouting();

            //The call to UseCors must be placed after UseRouting, but before UseAuthorization.
            app.UseCors();//CLIENTS_CORS_POLICY_NAME

            //app.UseHttpsRedirection(); // after UseCors();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                //SignalR
                endpoints.MapHub<CustomersHub>("/hubs/customers");
                //.RequireCors(CLIENTS_CORS_POLICY_NAME);

                //gRPC
                endpoints.MapGrpcService<CustomerServiceGRPC>();
            });
            
            dbInitializer.InitializeDb();
        }
    }
}