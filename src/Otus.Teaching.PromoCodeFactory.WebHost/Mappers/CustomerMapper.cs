﻿﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Grpc = Otus.Teaching.PromoCodeFactory.WebHost.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Google.Rpc.Context.AttributeContext.Types;
using Otus.Teaching.PromoCodeFactory.WebHost.Utils;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class CustomerMapper
    {

        public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if(customer == null)
            {
                customer = new Customer();
                customer.Id = Guid.NewGuid();
            }
            
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            

            return customer;
        }

        public static CreateOrEditCustomerRequest MapFromModel(Grpc::Customer grpcModel) //CreateCustomerRequest
        {
            return new CreateOrEditCustomerRequest()
            {
                FirstName = grpcModel.FirstName,
                LastName = grpcModel.LastName,
                Email = grpcModel.Email,
                PreferenceIds = grpcModel.PreferenceIds
                                    .Select(p => p.ToGuid())
                                    .ToList(),
            };

        }
    }
}
