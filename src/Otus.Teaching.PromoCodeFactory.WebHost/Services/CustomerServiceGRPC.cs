﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.WebHost.Protos;
using Models = Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Otus.Teaching.PromoCodeFactory.WebHost.Protos.CustomerService;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Utils;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomerServiceGRPC: CustomerServiceBase
    {
        private readonly CustomerService _customerSrv;

        //ctor
        public CustomerServiceGRPC(CustomerService customerService)
        {
            _customerSrv = customerService;
        }
        //Get All
        public override async Task<ListCustomerShortResponse> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerSrv.GetCustomersAsync();

            var custList = customers.Select(customer => new CustomerShortResponse
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
            })
                           .ToList();

            var response = new ListCustomerShortResponse();
            response.Customers.AddRange(custList);

            return response;
        }

        //Get by id
        public override async Task<CustomerResponse> GetCustomer(GetOrDeleteCustomerRequest request, ServerCallContext context)
        {
            //get customer
            var customer = await _customerSrv.GetCustomerAsync(request.Id.ToGuid());

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, $"Customer with id='{request.Id}' not found"));

            var custRespREST = new Otus.Teaching.PromoCodeFactory.WebHost.Models.CustomerResponse(customer);

            var response = new CustomerResponse()
            {
                Id = request.Id,
                FirstName = custRespREST.FirstName,
                LastName = custRespREST.LastName,
                Email = custRespREST.Email,
            };

            //add prefs
            if (custRespREST.Preferences != null)
            {
                foreach (var pref in custRespREST.Preferences)
                {
                    response.Preferences.Add(new PreferenceResponse()
                    {
                        Id = pref.Id.ToString(),
                        Name = pref.Name
                    });
                }
            }

            //add promocodes
            if (custRespREST.PromoCodes != null)
            {
                foreach (var promo in custRespREST.PromoCodes)
                {
                    response.PromoCodes.Add(new PromoCodeShortResponse()
                    {
                        Id = promo.Id.ToString(),
                        Code = promo.Code,
                        ServiceInfo = promo.ServiceInfo,
                        BeginDate = promo.BeginDate,
                        EndDate = promo.EndDate,
                        PartnerName = promo.PartnerName,
                    });
                }
            }

            return response;
        }

        //Create
        public override async Task<CustomerResponse> CreateCustomer(CustomerRequest request, ServerCallContext context)
        {
            var customerDto = CustomerMapper.MapFromModel(request.Customer);

            //create new customer
            var customer = new Models::CustomerResponse(
                                await _customerSrv.CreateCustomerAsync(customerDto));


            var response = new CustomerResponse()
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,

            };

            //add prefs
            foreach (var pref in customer.Preferences)
                response.Preferences.Add(new PreferenceResponse()
                {
                    Id = pref.Id.ToString(),
                    Name = pref.Name
                });

            return response;
        }

        //Edit
        public override async Task<Empty> EditCustomer(CustomerRequest request, ServerCallContext context)
        {            
            var customerDto = CustomerMapper.MapFromModel(request.Customer);

            await _customerSrv.EditCustomersAsync(request.Customer.Id.ToGuid(), customerDto);

            return new Empty();
        }

        //Delete
        public override async Task<Empty> DeleteCustomer(GetOrDeleteCustomerRequest request, ServerCallContext context)
        {
            var isDeleted = await _customerSrv.DeleteCustomerAsync(request.Id.ToGuid());
            if (!isDeleted)
                throw new RpcException(new Status(StatusCode.Unknown, $"Customer with id='{request.Id}' was not deleted"));


            return new Empty();
        }

    }
}
