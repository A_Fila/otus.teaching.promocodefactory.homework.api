﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        //private readonly IRepository<Preference> _preferencesRepository;
        private readonly CustomerService _customerSrv;

        public PreferencesController(//IRepository<Preference> preferencesRepository
                                     CustomerService customerService)
        {
            //_preferencesRepository = preferencesRepository;
            _customerSrv = customerService;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            //var preferences = await _preferencesRepository.GetAllAsync();

            //var response = preferences.Select(x => new PreferenceResponse()
            //{
            //    Id = x.Id,
            //    Name = x.Name
            //}).ToList();
            var response = await _customerSrv.GetPreferencesAsync();

            return Ok(response);
        }
    }
}