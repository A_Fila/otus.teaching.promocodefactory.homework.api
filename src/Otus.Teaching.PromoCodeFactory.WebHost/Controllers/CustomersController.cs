﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly CustomerService _customerSrv;

        public CustomersController(IRepository<Customer> customerRepository, 
                                   IRepository<Preference> preferenceRepository,
                                   CustomerService customerService)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _customerSrv = customerService;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            //var customers =  await _customerRepository.GetAllAsync();

            //var response = customers.Select(x => new CustomerShortResponse()
            //{
            //    Id = x.Id,
            //    Email = x.Email,
            //    FirstName = x.FirstName,
            //    LastName = x.LastName
            //}).ToList();

            var response = await _customerSrv.GetCustomersAsync();

            return Ok( response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Id клиента, например: <example>d54f3eff-5b25-42f2-bdea-067ec5130dc5</example></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]        
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //var customer =  await _customerRepository.GetByIdAsync(id);
            //var response = new CustomerResponse(customer);

            var customer = await _customerSrv.GetCustomerAsync(id);
            
            if(customer == null) 
                return NotFound();
                        

            return Ok(new CustomerResponse(customer));
        }
        
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            ////Получаем предпочтения из бд и сохраняем большой объект
            //var preferences = await _preferenceRepository
            //    .GetRangeByIdsAsync(request.PreferenceIds);

            //Customer customer = CustomerMapper.MapFromModel(request, preferences);
            
            //await _customerRepository.AddAsync(customer);
            Customer customer = await _customerSrv.CreateCustomerAsync(request);


            return CreatedAtAction(nameof(GetCustomerAsync), new {id = customer.Id}, customer.Id);
        }
        
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //var customer = await _customerRepository.GetByIdAsync(id);
            
            //if (customer == null)
            //    return NotFound();
            
            //var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            
            //CustomerMapper.MapFromModel(request, preferences, customer);

            //await _customerRepository.UpdateAsync(customer);

            var isEdited = await _customerSrv.EditCustomersAsync(id, request);

            return (isEdited != null) ? NoContent()
                                      : NotFound();
        }
        
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            //var customer = await _customerRepository.GetByIdAsync(id);

            //if (customer == null)
            //    return NotFound();

            //await _customerRepository.DeleteAsync(customer);

            if(! await _customerSrv.DeleteCustomerAsync(id))
                return NotFound(); 

            return NoContent();
        }
    }
}