﻿using Grpc.Core;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Utils
{
    public static partial class StringExtensions
    {
        public static Guid ToGuid(this string id)
        {
            if (!Guid.TryParse(id, out Guid guid))
                throw new RpcException(new Status(StatusCode.InvalidArgument, $"Invalid GUID format: '{id}'"));

            return guid;
        }
    }
}
